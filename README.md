# PB0812 Cross-sectioneel onderzoek Oefenopdracht

Dit is een GitLab repository met alle data en analysescripts voor de Oefenopdracht van Onderzoekspracticum cross-sectioneel onderzoek (cursus PB0812 bij de Open Universiteit).

Het Open Science Framework repository voor dit project is https://osf.io/ztmq3/ en het bijbehorende GitLab repository is https://gitlab.com/matherion/pb0812-cross-sectioneel-onderzoek-oefenopdracht. De gerenderde versie van dit RMarkdown bestand wordt gehost door GitLab Pages op https://matherion.gitlab.io/pb0812-cross-sectioneel-onderzoek-oefenopdracht.
